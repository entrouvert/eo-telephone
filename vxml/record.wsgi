import site
import sys
import os

prev_sys_path = list(sys.path) 

site.addsitedir('/home/bdauvergne/venv/lib/python2.7/site-packages/')
# Reorder sys.path so new directories at the front.
new_sys_path = [] 
for item in list(sys.path): 
    if item not in prev_sys_path: 
        new_sys_path.append(item) 
        sys.path.remove(item) 
sys.path[:0] = new_sys_path 

from flask import Flask, request, Response, __version__
from werkzeug.debug import DebuggedApplication
from werkzeug.urls import url_encode

import os
import hashlib
import requests
import subprocess

secret = 'secret'

app = Flask(__name__)
app.debug = True
application = DebuggedApplication(app, evalex=True)    # The trick is HERE! Add this extra line!

@app.route("/", methods=['GET', 'POST'])
def record():
    if request.method == 'POST':
        f = request.files['file']
        path = '/tmp/'
        f.save(os.path.join(path, f.filename))
    return Response('OK')
