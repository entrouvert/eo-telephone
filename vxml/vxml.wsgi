import site
import sys

prev_sys_path = list(sys.path) 

site.addsitedir('/home/bdauvergne/venv/lib/python2.7/site-packages/')
# Reorder sys.path so new directories at the front.
new_sys_path = [] 
for item in list(sys.path): 
    if item not in prev_sys_path: 
        new_sys_path.append(item) 
        sys.path.remove(item) 
sys.path[:0] = new_sys_path 

from flask import Flask, request, Response, __version__
from werkzeug.debug import DebuggedApplication
from werkzeug.urls import url_encode

import os
import hashlib
import requests
import subprocess

secret = 'secret'

app = Flask(__name__)
app.debug = True
application = DebuggedApplication(app, evalex=True)    # The trick is HERE! Add this extra line!

default_voice = 'Fabienne'

@app.route("/play")
def play():
    text = request.args.get('text')
    voice = request.args.get('voice', default_voice)
    key_prehash = secret + voice + text
    key = hashlib.md5(key_prehash.encode('utf8')).hexdigest()
    path = os.path.join('sounds', key+'.wav')
    if not os.path.exists(path):
        r = requests.get('http://voxygen.fr/sites/all/modules/voxygen_voices/assets/proxy/index.php',
               params=dict(method='redirect', text=text, voice=voice))
        p = subprocess.Popen('/usr/bin/sox -t mp3 - -r 8k -c 1 -t wav -'.split(), stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate(input=r.content)
        with file(path, 'w') as f:
            f.write(out)
    return Response(file(path), mimetype="audio/x-wav")

import xml.etree.ElementTree as ET

@app.route("/vxml/<path:path>", methods=['GET', 'POST'])
def vxml(path, voice=default_voice):
    tree = ET.parse(path)
    root = tree.getroot()
    if request.form:
        for node in root.iter():
                for key in node.attrib:
                    node.attrib[key] = node.attrib[key].format(**request.form)
    for audio in root.findall('.//{http://www.w3.org/2001/vxml}audio'):
        play_voice = voice
        if 'voice' in audio.attrib:
            play_voice = audio.attrib['voice']
            del audio.attrib['voice']
        q = dict(voice=play_voice, text=audio.text)
        url = '/~bdauvergne/vxml.wsgi/play?%s' % url_encode(q)
        audio.attrib['src'] = url
    return Response(ET.tostring(root), mimetype='application/xml')
